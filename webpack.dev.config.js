const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.config.js');
const CleanWebpackPlugin = require('clean-webpack-plugin');

var createWebpackConfig = function(outputPath) {
  return {
    output: {
      path: path.join(outputPath, '/priv'),
      publicPath: '/'
    },
    plugins: [
      new CleanWebpackPlugin(['priv'], {
          root:     outputPath,
          verbose:  true,
          dry:      false
        })
    ],
    devtool: 'inline-source-map',
    devServer: {
      contentBase: './priv'
    }
  }
}

module.exports = merge(common, createWebpackConfig(path.join(__dirname, '../wui2')));
