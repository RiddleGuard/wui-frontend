import _ from 'lodash';
import PropTypes from 'prop-types';

let Colous = null;

const render_rows = (rows) => {
  if (rows)
   return rows.map(function (row) {
    return(
      <tr>
        {(() => {
          let result = [];
          for (let key in row)
            Colous.map((key_saved) =>
              (key_saved == key) ? result.push(<td>{row[key]}</td>) : null)
          return result;
        })()}
      </tr>
    )
  });
}

const fmt_cols = (arr_cols) => {
  return arr_cols.map((item) => <th>{item}</th>)
}

const push_cols = (clouns) => {
  let result = [];
  Colous = _.keys(clouns);
  for (let key in clouns) {
    result.push(clouns[key]);
  }
  return result;
}

const render_cols = (cols,rows) => {
  let res = [];
  if (cols.length !== 0)
    return push_cols(cols)
  else
    if (rows){
      return push_cols(rows[0])
    }
  return res;
}


const total_sum_for_def_tabel = (cols, rows) => {
  let result = [];
  let iter = 0;
  if (cols.length == 0) {
    cols = render_cols(cols,rows)
  }
  for (let key in cols) {
    if (iter == 0)
      result.push(<td>Total {rows.length.toString()}</td>);
    else
      result.push(<td>{total_sum_col(key, rows)}</td>);
    iter ++;
  }
  return <tr>{result}</tr>;
}


const total_sum_col = (key, rows) => {
  let sum = 0;
  for (let records in rows) {
    if (_.isNumber(rows[records][key]))
      sum += rows[records][key];
    else
      return '';
  }
  return sum;
}

export default class TableDefault extends React.Component {
  render() {
    const {rows, cols, className, config} = this.props;
    return (
      <table className={className}>
      <thead>
        <tr>
          {fmt_cols(render_cols(cols,rows))}
        </tr>
      </thead>
      <tbody>
        {render_rows(rows)}
      </tbody>
        {(config.total_sum == true) ?
          <tfoot>
            {total_sum_for_def_tabel(cols,rows)}
          </tfoot> : null}
      </table>
    );
  }
}
