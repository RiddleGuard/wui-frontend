import Tabs from 'muicss/lib/react/tabs';
import Tab from 'muicss/lib/react/tab';
import HKey from './hkey';

const TabPanel = ({data, namespace, className, config, updata_tabs}) => {
  if (data)
    return (
      <div className={"row mb-0 " + className}>
        <Tabs defaultSelectedIndex={1} justified={true}>
          {renderTabs(data, updata_tabs)}
        </Tabs>
      </div>
    );
}

const renderTabs = (data, updata_tabs, isLoading) => {
  let result = [];
  let col = data.length;
  for (let i = 0; i < col; i++)
    result.push(
      <Tab label={(data[i] && data[i].name) ? data[i].name : "Tab"}>
        {data[i] && <HKey data={data[i]}
                          updata={updata_tabs}
                          isLoading={isLoading}/>}
      </Tab>)
  return result;
}

export default TabPanel;
