const Memo = ({data, name, className, config}) => {
  return (
    <div className={className}>
      <h5>{name}</h5>
      <pre style={(config.style) ? config.style : null}>{data}</pre>
    </div>
  );
}
export default Memo;
