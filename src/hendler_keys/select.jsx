import Select from 'react-select';
import 'react-select/dist/react-select.css';
import HKey from './hkey';

export default class SelectBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    }
    this.UPDATE_STATE_SELECT = new Event("UPDATE_STATE_SELECT");
  }

  OnChange (config, val) {
    document.dispatchEvent(this.UPDATE_STATE_SELECT);
    if (val) {
      this.updata({value: val.value});
      this.props.updata_select({...val, ...config, name: this.props.namespace});
    } else {
      this.updata({value: null});
      this.props.updata_select({value: null,link: null, label: null, name: null});
    }
  }

  updata(update) {
    this.setState({...this.state,...update});
  }

  componentDidMount() {
    let defaultValue  = "";
    if (this.props.data)
      this.updata({
        value: this.props.data[0] && this.props.data[0].value || defaultValue});
  }

  // psevdo_refresh() {
  //   if (this.props.config)
  // }

  render() {
    if (this.props.data)
      return (
          <Select
            name={this.props.namespace}
            value={this.state.value || ''}
            options={this.props.data}
            onChange={this.OnChange.bind(this, this.props.config)}/>
        )
    else return null
  }
}
