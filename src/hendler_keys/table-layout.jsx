import HKey from './hkey';
import PropTypes from 'prop-types';

const gridCol = (i) => {
  return {
    position: 'relative',
    width: '100%',
    minHeight: '1px',
    paddingRight: '15px',
    paddingLeft: '15px',
    flex: `0 0 ${i}%`,
    maxWidth: `${i}%`,
    overflow: 'hidden'
  }
};

export default class TableLayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hideNav: false
    };
  };

  static propTypes = {
    rows: PropTypes.array,
    cols: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    className: PropTypes.string,
    config: PropTypes.object,
    updata: PropTypes.func,
    isLoading: PropTypes.bool
  };

  componentDidMount() {
    window.addEventListener("resize", this.resize);
    this.resize();
  };

  componentWillUnmount() {
    window.removeEventListener("resize", this.resize);
  }

  resize = () => {
    this.setState({hideNav: window.innerWidth <= 900});
  };

  render() {
    const {rows, cols, className, config, updata, isLoading} = this.props;
    return(
      <div className={`row mb-0 ${className}`}>
        {(() => {
          let result = [];
          for (let i = 0; i < config.col; i++) {
            result.push(
              <div
                style={gridCol(this.state.hideNav && 100 || 100/config.col)}
                className="col-sm-12 mb-0">
                  {rows[i] && <HKey data={rows[i]}
                                    updata={updata}
                                    isLoading={isLoading}/>}
              </div>)
          }
          return result;
        })()}
      </div>
    );
  }
};
