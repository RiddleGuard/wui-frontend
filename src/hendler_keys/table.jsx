import TableLayout from './table-layout';
import TableExtend from './table-extend';
import TableDefault from './table-default';
import PropTypes from 'prop-types';

const Table = (props) => {
  switch (props.config.mode) {
    case 'layout':
      return <TableLayout {...props}/>
      break;
    case 'extended':
      return <TableExtend {...props}/>
    default:
      return <TableDefault {...props}/>
  }
}

Table.propTypes = {
  config: PropTypes.shape({
    mode: PropTypes.oneOf(['layout', 'extended', 'default']),
    total_sum: PropTypes.boolean}).isRequired,
  className: PropTypes.string,
  prefix: PropTypes.string,
  namespace: PropTypes.string,
  rows: PropTypes.arrayOf([PropTypes.object]),
  cols: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  updata: PropTypes.func
};

export default Table;
