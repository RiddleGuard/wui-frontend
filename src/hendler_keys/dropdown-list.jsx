import 'bootstrap/js/dist/collapse';
import _ from 'lodash';
import PaginationData from './pagination';
import { json_type_of } from './../utils/utils';

const DefaultPagi = {
  activePage : 1,
  perPage: 5
}

const render_rows = (rows) => {
  if (rows)
    return rows.map((row) =>
      <tr>
        <th>{row.name}</th>
        <th>{row.val}</th>
      </tr>
    )
}

const DropdownList = ({rows, prefix = '', config="default", specClassName=''}) => {
  switch (config.mode) {
    case 'extended':
      let activePage = (!isNaN(config.active_page)) ? config.active_page : DefaultPagi.activePage;
      let perPage = (!isNaN(config.per_page)) ? config.per_page :  DefaultPagi.perPage;
      return (
      <PaginationData
        data={rows}
        render={render_drop_down_list}
        activePage={activePage}
        perPage={perPage}/>);
      break;
    case undefined:
    default:
      return (
        <div className="accordion">
          <div className={`accordion-group drop-down-list ${specClassName}`}>
            <ul>
              {controller_ddl(rows, prefix)}
            </ul>
          </div>
        </div>
      );
  }
}

const RenderDDL = ({list, prefix ='', className}) => {
  let result = [];
  let index = 0;
  if (list && _.isObject(list)) {
    for(let key in list) {
      if (_.isArray(list[key]))
        result.push(<RenderAcordion key={key} link={index + prefix} name={key} value={list[key]}/>)
      else
        if(_.isNumber(list[key]) || list[key])
          result.push(<li key={key}><span className="ddl-name">{key}:</span> <span className="ddl-value">{list[key]}</span></li>)
      index++;
    }
  }
  return result;
}

class RenderAcordion extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toggole: false,
      is_refresh: false
    }
  }

  heandler_toggle_data (e) {
    e.preventDefault();
    e.stopPropagation();
    this.setState({
          toggole: !this.state.toggole,
          is_refresh: false
        })
  }

  toggole() {
    if (!this.state.toggole)
      return (
        <span>
          <a className="accordion-toggle ddl-link" data-toggle="collapse" href={'#' + this.props.link} onClick={this.heandler_toggle_data.bind(this)}>
            <span className="fa fa-caret-up rotate-90 ddl-arrow"></span>
            <span className="ddl-truncate radius-3 p-1">{_.truncate(JSON.stringify(this.props.value), {'length': 24,'omission': ' [...]'})}</span>
          </a>
        </span>)
    else
      return(
        <a className="accordion-toggle" data-toggle="collapse" href={'#' + this.props.link} onClick={this.heandler_toggle_data.bind(this)}>
          <span className="fa fa-caret-down ddl-arrow"></span>
        </a>)
  }

  render () {
    return (
      <div className="accordion">
        <div className="accordion-group">
          <li>
              <span className="ddl-name">{this.props.name}: </span>
              {this.toggole()}
          </li>
          <div className="accordion-body collapse" id={this.props.link}>
            <div className="accordion-inner">
              <ul>
                {controller_ddl(this.props.value, this.props.link)}
              </ul>
            </div>
          </div> {/*accordion-body collapse*/}
        </div> {/*accordion-group*/}
      </div>
    );
  }
}

function render_drop_down_list(obj) {
    return obj.map((todo, index) => <DropdownList key={index}
                                                  rows={todo}
                                                  prefix={"item"+index}
                                                  specClassName="ddl-array"/>);
  }

function controller_ddl(unknown, prefix='') {
  switch (json_type_of(unknown)) {
    case "Array":
      return unknown.map((elem_arr, index) => <div className="ddl-array radius-3"><RenderDDL key = {index} list = {elem_arr} prefix={index + prefix}/></div>);
      break;
    case "Object":
      return <RenderDDL prefix={prefix} list={unknown}/>;
      break;
  }
}

export default DropdownList;
