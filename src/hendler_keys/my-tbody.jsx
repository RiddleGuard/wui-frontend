import _ from 'lodash';
import { json_type_of as JsonTypeOf, set} from './../utils/utils';
import DropdownTable, {totalSum} from './dropdown-table';
import ReactDOM from 'react-dom';

const IdItem = {
  SHADOW: 'SHADOW_LIST',
  LIST: 'LIST'
}

const SHADOW = (id) => `${IdItem.SHADOW}_${id}`;
const LIST = (index,Parent) => `${IdItem.LIST}_${index}&&PARENT_${Parent}`;

class MyTbody extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toggole: false,
      is_refresh: false,
      UpClouns: null
    }
  }
// style={(this.state.toggole) ? {padding: "5px 10px"} : null}
  render () {
    return (
    <MultiTbody>
      <tbody className={(isData(this.props.row)) ? "ddt-row-down " : "non"}>
        <tr className="clickable"
            onClick={this.heandlerToggleData
                      .bind(this, this.props.prefix, this.props.row)}>
          {this.renderRow(this.props.row)}
        </tr>
      </tbody>
      <tbody id={SHADOW(this.props.prefix)}
            className={((isData(this.props.row)) ?
              "ddt-row-down "
              : "non") + " ddt-child-table p-0"}/>
    </MultiTbody>);
  }

  heandlerToggleData (key, obj, e) {
    e.preventDefault();
    e.stopPropagation();
    if(!this.state.toggole){
      if (isData(obj)) {
        let Elem = document.getElementById(SHADOW(key));
        ReactDOM.render(
          <Contoler prefix={SHADOW(key)}
                    config={this.props.config}
                    keys_header={this.props.keys_header}>{obj}</Contoler>
          ,Elem);
      }
    } else {
      ReactDOM.unmountComponentAtNode(
        document.getElementById(SHADOW(key)));
    }
    this.setState({
          toggole: !this.state.toggole,
          is_refresh: false
        });
  }

  renderRow (row) {
    let keys = this.props.keys_header;
    let res = [];
    for (let key in keys) {
      if (!_.isObject(row[keys[key]]) &&
          !_.isArray(row[keys[key]])) {
        if (row[keys[key]] !== "") {
          if (key === "0") {
            res.push (
              <td className="active rel">
                {row[keys[key]]}
                <div className={(isData(this.props.row)) ?
                  "arrow" : null}></div>
              </td>);

          } else {
            res.push (
              <td className="active">
                {row[keys[key]]}
              </td>);
          }
        } else
          res.push(<td></td>);
      }
    }
    return res;
  }
}

class RowList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toggole: false,
      is_refresh: false,
      UpClouns: null
    }
  }

  render () {
    return (
    <td className = "active rel"
        style = {{padding: '4px'}}
        role="button"
        onClick={this.heandlerToggleData.bind(this)}>
      {this.props.children}
      <div className={(isData(this.props.data)) ? "arrow" : null}></div>
    </td>);
  }

  heandlerToggleData (e) {
    e.preventDefault();
    e.stopPropagation();
    if(!this.state.toggole){
      if (isData(this.props.data)){
        let Elem = document.getElementById(SHADOW(this.props.prefix))
        ReactDOM.render(<Contoler prefix={this.props.prefix}
                                  config={{...this.props.config, total_sum: true}}
                                  keys_header={this.props.keys_header}
                                  parent="RowList">
                          {this.props.data}
                        </Contoler>
          ,Elem);
      }
    } else {
      ReactDOM.unmountComponentAtNode(
        document.getElementById(SHADOW(this.props.prefix)));
    }
    this.setState({
          toggole: !this.state.toggole,
          is_refresh: false
        });
  }
}

// =============================================================================
// Functions
// =============================================================================

function MultiTbody(props) {
  return props.children;
}

function isData(row) {
  for (let key in row)
    if ((_.isObject(row[key]) ||
          _.isArray(row[key])) &&
          row[key] !== "")
        return true
  return false
}

function Contoler(props) {
  switch (JsonTypeOf(props.children)) {
    case "Array":
      return <DropdownTable {...props}
                            rows={props.children}
                            prefix={props.prefix}
                            className="ddl-array radius-3"
                            config={{...props.config,  total_sum: false}}
                            parent="RowList"/>
      break;
    case "Object":
      let res = [];
      let row = props.children;
      let iterator = 0;
      let keys = props.keys_header;
      for (let key in row) {
        if ((_.isObject(row[key]) ||
              _.isArray(row[key])) &&
              row[key] && row[key] !== "") {
            res.push(<tr  colSpan="10"
                          style = {{backgroundColor: "#fff"}}>
              <RowList  prefix={LIST(iterator,props.prefix)}
                        data={row[key]}
                        config={props.config}
                        keys_header={keys}>
                {key}
              </RowList>
              {/* it is (_.keys(row[key])< keys) */}
              {(props.config.total_sum === true) ?
                totalSum(row[key], keys, "short")
                :
                null}
            </tr>);
            res.push(<tr><td colSpan="10"
                              id={SHADOW(LIST(iterator,props.prefix))}
                              className={((isData(row)) ?
                                "ddt-row-down "
                                : "non") + "p-0"}></td></tr>);
          iterator++;
        }
      }
      if (props.parent === "RowList")
        return (
          <table width="100%">
            {(() => {
              if (keys) {
                let response = [];
                let first = false
                for (let key in keys)
                  if (!first) {
                    response.push(<th>List</th>);
                    first = true;
                  } else
                    response.push(<th>{keys[key]}</th>)
                return <thead><tr>{response}</tr></thead>;
              } else
                return null;
            })()}
            <tbody>{res}</tbody>
          </table>)
      return res;
      break;
    default:
      return null;
  };
}

export {SHADOW, LIST}
export default MyTbody;
