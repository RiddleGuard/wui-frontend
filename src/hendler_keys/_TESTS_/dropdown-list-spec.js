var data =  {"interface": "VLAN0001",
            "permit": 0,
            "deny"  : 0,
            "rules":[
                {"pcl":"IPv4 ACL: test",
                "value":[
                          {"rule":"10 permit tcp any time any time log-input",
                            "counter":0}
                        ]}
            ],
            "name4": "value4"}

var data2 =  {"name1": "value1",
            "name2": "value2",
            "name3": "value3",
            "name4": "value4"}

var data3 =  {"name1": "value1",
            "name2": "value2",
            "rules": [{"pcl":"IPv4 ACL: test",
                        "key": "value"}],
            "rules2":"2",
            "name4": "value4"}

var data4 = {"interface":"VLAN0001","permit":0,"deny":0,"rules":[[["pcl","IPv4 ACL: test"],["value",[[["rule","10 permit tcp any time any time log-input"],["counter",0]]]]]]}

var data5 = {"interface":"VLAN0001",
"permit":0,
"deny":0,
"rules":[
  {"pcl":"IPv4 ACL: test",
  "value":[
    {"rule":"10 permit tcp any time any time log-input",
    "counter":0}
  ]},
  {"pcl":"IPv4 ACL: test2",
    "value":[
      {"rule":"10 permit tcp any time any time log-input",
      "counter":0}
  ]}
]}

var data7 = {"interface":"VLAN0001",
"permit":0,
"deny":0,
"rules":[
  {"pcl":"IPv4 ACL: test",
  "value":[
    {"rule":"10 permit tcp any time any time log-input",
    "counter":0}
  ]},
  {"pcl":"IPv4 ACL: test2",
    "value":[
      {"rule":"10 permit tcp any time any time log-input",
      "counter":0},
      {"rule":"11 permit tcp any time any time log-input",
      "counter":0}
  ]}
]}
var data6 = [{"interface":"VLAN0001",
"permit":0,
"deny":0,
"rules":[
  {"pcl":"IPv4 ACL: test",
  "value":[
    {"rule":"10 permit tcp any time any time log-input",
    "counter":0}
  ]},
  {"pcl":"IPv4 ACL: test2",
    "value":[
      {"rule":"10 permit tcp any time any time log-input",
      "counter":0},
      {"rule":"11 permit tcp any time any time log-input",
      "counter":0}
  ]}
]},
{"interface":"VLAN0002",
"permit":0,
"deny":0,
"rules":[
  {"pcl":"IPv4 ACL: test",
  "value":[
    {"rule":"10 permit tcp any time any time log-input",
    "counter":0}
  ]},
  {"pcl":"IPv4 ACL: test2",
    "value":[
      {"rule":"10 permit tcp any time any time log-input",
      "counter":0},
      {"rule":"11 permit tcp any time any time log-input",
      "counter":0}
  ]}
]},
{"interface":"VLAN0003",
"permit":0,
"deny":0,
"rules":[
  {"pcl":"IPv4 ACL: test",
  "value":[
    {"rule":"10 permit tcp any time any time log-input",
    "counter":0}
  ]},
  {"pcl":"IPv4 ACL: test2",
    "value":[
      {"rule":"10 permit tcp any time any time log-input",
      "counter":0},
      {"rule":"11 permit tcp any time any time log-input",
      "counter":0}
  ]}
]},
{"interface":"VLAN0003",
"permit":0,
"deny":0,
"rules":[
  {"pcl":"IPv4 ACL: test",
  "value":[
    {"rule":"10 permit tcp any time any time log-input",
    "counter":0}
  ]},
  {"pcl":"IPv4 ACL: test2",
    "value":[
      {"rule":"10 permit tcp any time any time log-input",
      "counter":0},
      {"rule":"11 permit tcp any time any time log-input",
      "counter":0}
  ]}
]},
{"interface":"VLAN0004",
"permit":0,
"deny":0,
"rules":[
  {"pcl":"IPv4 ACL: test",
  "value":[
    {"rule":"10 permit tcp any time any time log-input",
    "counter":0}
  ]},
  {"pcl":"IPv4 ACL: test5",
    "value":[
      {"rule":"10 permit tcp any time any time log-input",
      "counter":0},
      {"rule":"11 permit tcp any time any time log-input",
      "counter":0}
  ]}
]},
{"interface":"VLAN0001",
"permit":0,
"deny":0,
"rules":[
  {"pcl":"IPv4 ACL: test",
  "value":[
    {"rule":"10 permit tcp any time any time log-input",
    "counter":0}
  ]},
  {"pcl":"IPv4 ACL: test2",
    "value":[
      {"rule":"10 permit tcp any time any time log-input",
      "counter":0},
      {"rule":"11 permit tcp any time any time log-input",
      "counter":0}
  ]}
]},
{"interface":"VLAN0002",
"permit":0,
"deny":0,
"rules":[
  {"pcl":"IPv4 ACL: test",
  "value":[
    {"rule":"10 permit tcp any time any time log-input",
    "counter":0}
  ]},
  {"pcl":"IPv4 ACL: test2",
    "value":[
      {"rule":"10 permit tcp any time any time log-input",
      "counter":0},
      {"rule":"11 permit tcp any time any time log-input",
      "counter":0}
  ]}
]},
{"interface":"VLAN0003",
"permit":0,
"deny":0,
"rules":[
  {"pcl":"IPv4 ACL: test",
  "value":[
    {"rule":"10 permit tcp any time any time log-input",
    "counter":0}
  ]},
  {"pcl":"IPv4 ACL: test2",
    "value":[
      {"rule":"10 permit tcp any time any time log-input",
      "counter":0},
      {"rule":"11 permit tcp any time any time log-input",
      "counter":0}
  ]}
]},
{"interface":"VLAN0003",
"permit":0,
"deny":0,
"rules":[
  {"pcl":"IPv4 ACL: test",
  "value":[
    {"rule":"10 permit tcp any time any time log-input",
    "counter":0}
  ]},
  {"pcl":"IPv4 ACL: test2",
    "value":[
      {"rule":"10 permit tcp any time any time log-input",
      "counter":0},
      {"rule":"11 permit tcp any time any time log-input",
      "counter":0}
  ]}
]},
{"interface":"VLAN0004",
"permit":0,
"deny":0,
"rules":[
  {"pcl":"IPv4 ACL: test",
  "value":[
    {"rule":"10 permit tcp any time any time log-input",
    "counter":0}
  ]},
  {"pcl":"IPv4 ACL: test5",
    "value":[
      {"rule":"10 permit tcp any time any time log-input",
      "counter":0},
      {"rule":"11 permit tcp any time any time log-input",
      "counter":0}
  ]}
]}]

export {data6,data7,data5,data4,data3,data2};
