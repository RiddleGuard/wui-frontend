import _ from 'lodash';
import Table from './table';
import Window from './window';
import Memo from './memo';
import TabPanel from './tabs';
import SelectBox from './select';
import DropdownList from './dropdown-list';
import DropdownTable from './dropdown-table';
import Diagrama from './diagrama';

const HKey = ({data, updata, isLoading}) => {
  if (data) {
    let obj = search_obj(data);
    return obj.map(function (item) {
      switch (item.type) {
        case "table":
          return <Table className={(!item.config.className) ? "table table-striped table-hover table-sm" : item.config.className}
                  cols={item.data.cols}
                  rows={_.compact(item.data.rows)}
                  config={item.config}
                  namespace={item.namespace}
                  isLoading={isLoading}
                  updata={updata}/>
          break;
        case "window":
          return <Window rows={_.compact(item.data.rows)}
                  className={(!item.className) ? "window col-sm-12" : item.className}
                  name={item.data.name}
                  config={item.config}
                  namespace={item.namespace}
                  isLoading={isLoading}/>
          break;
        case "text":
          return <Memo data={_.compact(item.data)}
                  className={(!item.className) ? "" : item.className}
                  namespace={item.namespace}
                  isLoading={isLoading}
                  config={item.config}/>
          break;
        case "tabs_panel":
          return <TabPanel data={_.compact(item.data)}
                  className={(!item.className) ? "" : item.className}
                  namespace={item.namespace}
                  isLoading={isLoading}
                  updata_tabs = {updata}
                  config={item.config}/>
          break;
        case "select":
          return <SelectBox data={_.compact(item.data)}
                  updata_select = {updata}
                  className={(!item.className) ? "" : item.className}
                  namespace={item.namespace}
                  isLoading={isLoading}
                  config={item.config}/>
          break;
        case "drop_down_list":
          return <DropdownList rows={_.compact(item.data)}
                  className={(!item.className) ? "" : item.className}
                  namespace={item.namespace}
                  isLoading={isLoading}
                  config={item.config}/>
          break;
        case "drop_down_table":
          return <DropdownTable rows={_.compact(item.data)}
                  className={(!item.className) ? "" : item.className}
                  namespace={item.namespace}
                  isLoading={isLoading}
                  prefix={item.namespace}
                  config={item.config}/>
          break;
        case "diagrama":
          return <Diagrama data={_.compact(item.data)}
                  className={(!item.className) ? "" : item.className}
                  namespace={item.namespace}
                  isLoading={isLoading}
                  prefix={item.namespace}
                  config={item.config}/>
          break;
      }
    }.bind(updata))
  }
  return null;
}
export default HKey;

function search_obj(data) {
  let result = [];
  for (var key in data)
    if (_.isObject(data[key]) && ("type" in data[key]))
      result.push(data[key]);
  return result;
}


