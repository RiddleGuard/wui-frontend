// Reference Recharts - https://github.com/recharts/recharts
import {LineChart, Line, XAxis, YAxis, ReferenceLine,
CartesianGrid, Tooltip, Legend, Brush} from 'recharts';
import {map, compact, uniq, flatten, isNumber} from 'lodash';
import Colors from 'colors.json/colors';
import PropTypes from 'prop-types';

const DEFAULT_CNF = {
  width : 600,
  height: 300,
  margin: {top: 10, right: 30, left: 0, bottom: 0},
  minValue: 0,
  maxValue: 100
};

class CustomizedLabel extends React.Component {
  static propTypes = {
    labelRl: PropTypes.string
  };

  render() {
    const {
        fill, labelRl, textAnchor,
        fontSize, viewBox, dy, dx,
    } = this.props;
    const x = viewBox.x + 20;
    const y = viewBox.y - 2;
    return (
      <text
          x={x} y={y}
          dy={dy}
          dx={dx || 5}
          fill={fill}
          fontSize={fontSize || 10}
          className="custom-label"
          dominantBaseline="middle"
          textAnchor={textAnchor || 'middle'}>
          {labelRl}
      </text>
    )
  }
};

class CustomizedAxisTick extends React.Component {
  static propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    stroke: PropTypes.string,
    payload: PropTypes.shape({value: PropTypes.string})
  };

  render () {
    const {x, y, stroke, payload} = this.props;

    return (
      <g transform={`translate(${x},${y})`}>
        <text x={0} y={0} dy={16}
              textAnchor="end" fill="#666"
              transform="rotate(-35)">
          {payload.value}
        </text>
      </g>
    );
  }
};

export default class Diagrama extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.data,
      config: this.props.config
    };
  }

  static propTypes = {
    config: PropTypes.shape({
      width: PropTypes.number,
      height: PropTypes.number,
      margin: PropTypes.shape({
        top: PropTypes.number,
        right: PropTypes.number,
        left: PropTypes.number,
        bottom: PropTypes.number}),
      minValue: PropTypes.number.isRequired,
      maxValue: PropTypes.string.isRequired}).isRequired,
    className: PropTypes.string,
    prefix: PropTypes.string,
    namespace: PropTypes.string,
    isLoading: PropTypes.bool,
    data: PropTypes.shape({anotation: PropTypes.string.isRequired}).isRequired
  };

  static defaultProps = {config: DEFAULT_CNF};

  componentDidMount() {
    this.setState({
      ...this.state,
      data: this.props.data,
      config: this.props.config});
    document.addEventListener("UPDATE_STATE_SELECT", this.clearData, false);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.isLoading)
      this.handlerUpdate(nextProps.data);
  }

  componentWillUnmount() {
    document.removeEventListener('UPDATE_STATE_SELECT', this.clearData, false);
  }

  clearData = () => {
    //иначе если делать setState он оптимеирует его и пропускает шаг
    //очистки значений
    //Иногда он ругается на forceUpdate
    this.state.data = [];
    this.forceUpdate();
    // this.setState({...this.state, data: []});
  }

  setUpdate(update) {
    this.setState({...this.state, data: [...this.state.data, ...update]});
  }

  handlerUpdate(data) {
    this.setUpdate(this.filterLine(data, "line"));
  }

  filterLine(data, type) {
    return data.filter(line => line.type === type)
  }

  renderRefLine(data) {
    return data.map(line => <ReferenceLine y={line.value}
                            label={<CustomizedLabel labelRl={line.anotation}/>}
                            stroke="red"/>)
  }

  renderLine(data) {
    return data.map(function(data_key, index)
    {let color = getStepColor(index);
     return <Line type="monotone"
                  dataKey={data_key}
                  legendType={'line'}
                  dot={false}
                  stroke={color}/>});
  }

  render () {
    const config = this.state.config;
    const data = this.state.data;
    return (
      <LineChart width={config.width || DEFAULT_CNF.width}
                 height={config.height || DEFAULT_CNF.height}
                 data={this.filterLine(data, "line")}
                 margin={config.margin || DEFAULT_CNF.margin}>
        <XAxis dataKey="anotation" height={60} tick={<CustomizedAxisTick/>} />
        <YAxis domain={[config.minValue || DEFAULT_CNF.minValue,
                        config.maxValue || DEFAULT_CNF.maxValue]}/>
        <CartesianGrid strokeDasharray="3 3"/>
        <Tooltip />
        <Legend />
        {(() => {return this.renderRefLine(
          this.filterLine(data, "reference_line"))})()}
        {(() => {return this.renderLine(getDataKeys(
          this.filterLine(data, "line")))})()}
        <Brush dataKey="anotation"
               height={20}
               data={this.filterLine(data, "line")}/>
      </LineChart>
    );
  }
}

function getDataKeys(data) {
  let result = data.map((line_y) =>
    map(line_y, (value, index) => isNumber(value) ? index : null));
  return uniq(compact(flatten(result)));
}

function getStepColor(num) {
  let col = getKey(Colors, num + 10);
  return rgb(col[0], col[1], col[2]);
}

function getKey(obj, number){
  let i = 0;
  for(let key in obj) {
    if (i === number)
      return obj[key];
    i++;
  }
}

function rgb(r, g, b)
  {return "#" + dec2hex(r) + dec2hex(g) + dec2hex(b)};

function dec2hex(d){
  if(d > 15) return d.toString(16)
  else return "0"+d.toString(16)
}
