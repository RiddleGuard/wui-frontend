import PropTypes from 'prop-types';

const render_rows = (rows) => {
  if (rows)
    return rows.map((row) =>
      <tr>
        <th>{row.name}</th>
        <th><span className="word-break">{row.val}</span></th>
      </tr>
    )
}

export default class Window extends React.Component {
  constructor(props) {
    super(props);
    this.state = {buttonToggle: true};
  }

  static propTypes = {
    config: PropTypes.shape({toggle: PropTypes.bool}).isRequired,
    className: PropTypes.string,
    classNameIcon: PropTypes.string,
    namespace: PropTypes.string,
    name: PropTypes.string, // title
    rows: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string,
      val: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
      ])})).isRequired
  };

  static defaultProps = {
    config: {toggle: true}
  }

  renderSwitchWindow = () => {
    return(
    <a onClick={this.handleClick}>
      <span className={this.state.buttonToggle &&
                             "fa fa-minus-square-o" ||
                             "fa fa-plus-square-o"}></span>
    </a>);
  }

  handleClick = () => {
    this.setState({buttonToggle: !this.state.buttonToggle})
    return 0;
  }

  render() {
    return (
      <div className={this.props.className}>
        <h5>
          {this.props.classNameIcon &&
            <span className={this.props.classNameIcon}></span>}
          {this.props.name}
          {this.props.config.toggle != false &&
            <div className ="abs button-manager">
              {this.renderSwitchWindow()}
            </div>}
        </h5>
        <div style={{display: (this.state.buttonToggle ? 'block' : 'none')}}>
        <table className="table table-striped table-hover table-sm">
          <tbody>
            {render_rows(this.props.rows)}
          </tbody>
        </table>
        </div>
      </div>
  )};
};
