import _ from 'lodash';
import PaginationData from './pagination';
import MyTbody, {SHADOW, LIST} from './my-tbody';
import { json_type_of as JsonTypeOf} from './../utils/utils';
import ReactDOM from 'react-dom';

var CONFIG = {
  mode: "default",
  active_page : 1,
  per_page: 5,
  total_sum: false
}

var Couls = null;

const pushCols = (clouns) => {
  let result = [];
  for (let key in clouns) {
    result.push(clouns[key]);
  }
  return result;
}

const pushKeys = (clouns) => {
  let result = [];
  for (let key in clouns) {
    if (!_.isObject(clouns[key]) &&
        !_.isArray(clouns[key]) &&
        !isEmptyStr(clouns[key])){
      result.push(key);
    }
  }
  return result;
}

const fmtCols = (arr_cols) => {
  return arr_cols.map((item) => <th>{item}</th>)
}

const renderCols = (cols,rows) => {
  let res = [];
  if (cols && cols.length !== 0)
    return pushCols(cols)
  else
    if (rows){
      return pushKeys(rows[0])
    }
  return res;
}

// Требуется размонтирование при удалении из DOM
const UnmountAllChildTable =  (obj, Page) => {
  for (var i = 0; i <= obj.length; i++){
    let Elem = document.getElementById(SHADOW(`ITEM_AP${Page}_MT${i}`));
    if (Elem !== null) {
      ReactDOM.unmountComponentAtNode(Elem)
    }
  }
}

const renderDropDownList = (obj,state) => {
  if (state && state.previous !== null) {
    UnmountAllChildTable(obj, state.previous)
  }
  state.prefix +=`ITEM_AP${state.activePage}`;
  return <table className={`table table-striped table-hover table-sm ddt ddl-array`}>
    <thead>
      <tr>
      {fmtCols(renderCols([], obj))}
      </tr>
    </thead>
      {obj.map((row, index) =>
        <MyTbody row = {row} rows={obj} prefix = {`${state.prefix}_MT${index}`} keys_header={renderCols([], obj)} config={state.config}/>
      )}
      {(state.config.total_sum === true) ?
        <tfoot>
          <tr style={{backgroundColor: "#ccc"}}>{totalSum(state.data)}</tr>
        </tfoot> : null}
  </table>
}

const totalSum = (rows, keys=[], mode="full",td=true) => {
  let res = [];
  let res2 =[];
  if (keys.length === 0)
    keys = totalCols(rows)
  if (keys){
    let first = false;
    let listKeys = disabledBadCols(rows);
    for (let key in listKeys) {
      let S = 0;
      if (!first) {
        if (mode !== "short")
        res.push(<td></td>);
        first = true;
      } else {
        if (listKeys[key] !== null) {
          if (isElem(listKeys[key], keys)) {
            for (let row in rows){
              if (_.isNumber(rows[row][listKeys[key]]))
                S += rows[row][listKeys[key]];
            }
            res.push(<td>{S}</td>);
            res2.push(S);
          } else {
            if (mode !== "short")
            res.push(<td></td>)
          }
        }
      }
    }
  }
  if (td)
    return res;
  else
    return res2
}

const disabledBadCols = (rows) => {
  let col = 0;
  let res = [];
  for (let key in rows[col])
    if (!_.isObject(rows[col][key]) &&
        !_.isArray(rows[col][key]) &&
        !isEmptyStr(rows[col][key]))
      res[key] = key;
    else
      res[key] = null;
  return res;
}

const totalCols = (rows) => {
  let result = [];
  if (rows) {
    let listKeys = _.keys(rows[0]);
    for (var key in listKeys) {
      let res = [];
      for (let row in rows) {
        res.push(rows[row][listKeys[key]]);
      }
      if (isArrOfNumbers(res))
        result.push(listKeys[key]);
    };
  }
  return result;
}


const DropdownTable = ({cols, rows, prefix = 'ddt', namespace, config="default", specClassName='', className=""}) => {
  CONFIG = {...CONFIG, ...config};
  switch (CONFIG.mode) {
    case 'extended':
        let activePage = CONFIG.active_page;
        let perPage = CONFIG.per_page;
        return (
        <PaginationData
              data={rows}
              render={renderDropDownList}
              activePage={activePage}
              perPage={perPage}
              config={{...CONFIG, mode: "default"}}
              prefix={prefix}/>);
        break;
    case undefined:
    default:
      return (
        <table className={`table table-striped table-hover table-sm ddt ${specClassName} ${className}`}>
        <thead>
          <tr>
          {fmtCols(renderCols(cols, rows))}
          </tr>
        </thead>
          {rows.map((row, index) =>
            <MyTbody row = {row} rows={rows} prefix = {prefix + index} keys_header={renderCols(cols, rows)} config={CONFIG}/>
          )}
          {(CONFIG.total_sum === true) ?
            <tfoot>
              <tr style={{backgroundColor: "#ccc"}}>{totalSum(rows)}</tr>
            </tfoot> : null}
        </table>
      )
  }
}

// =============================================================================
// Functions
// =============================================================================
function isArrOfNumbers(arr) {
  for (var int = 0; int < arr.length; int++) {
    if (!_.isNumber(arr[int]))
      if (!(isEmptyStr(arr[int]) || isNaN(arr[int])))
      return false
  }
  return true
}

function isElem(elem, list) {
  return (list.indexOf(elem) !== -1)
}

function isEmptyStr(arg) {
  if (arg === "") return true;
  return false;
}

export { totalSum }
export default DropdownTable;
