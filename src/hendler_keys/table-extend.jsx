import FilterableTable from './../react-filterable-table/src/Components/FilterableTable';
import PropTypes from 'prop-types';

const addTotalSum = (cols) => {
  let result = [];
  let first = true;
  for (let key in cols)
    if (first) {
      first=false;
      result.push({...cols[key],
                  footerValue: (colouns) =>
                    "Total " + colouns.records.length.toString()});
    } else
      result.push({...cols[key], footerValue: total_sum});
  return result;
}

const renderColsExtended = (cols) => {
  if (cols) {
    let result = [];
    let first = true;
    for (let key in cols) {
      if(cols[key] === null){
        result.push({name: key, displayName: cols[key]});
        first = false;
      } else {
        let CommonField = {
          name: key,
          displayName: cols[key],
          inputFilterable: true,
          sortable: true}
        if (first) {
          first = false;
          result.push(CommonField);
        } else {
          result.push({
            ...CommonField,
            exactFilterable:true,
            visible: true});
        }
      }
    }
    return result;
  }
  return [];
}

const total_sum = (cols) => {
  let sum = 0;
  for (let key in cols.records) {
    if (_.isNumber(cols.records[key][cols.field.name]))
      sum += cols.records[key][cols.field.name];
    else {
      return '';
    }
  }
  return sum.toString();
}

export default class TableExtend extends React.Component {
  static propTypes = {
    rows: PropTypes.array,
    cols: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    className: PropTypes.string,
    config: PropTypes.shape({total_sum: PropTypes.bool}),
  };

  static defaultProps = {
      config: {total_sum: false}
  }

  render() {
    const {rows, cols, namespace, className, config} = this.props;
       // return 0;
    switch (config.total_sum) {
      case true:
        return (
          <FilterableTable
            namespace={namespace}
            initialSort="iface"
            data={rows}
            fields={addTotalSum(renderColsExtended(cols))}
            noRecordsMessage="Not found!"
            noFilteredRecordsMessage="Not found!"
            tableClassName={className}/>
        );
        break;
      default:
        return (
           <FilterableTable
              namespace={namespace}
              initialSort="iface"
              data={rows}
              fields={renderColsExtended(cols)}
              noRecordsMessage="Not found!"
              noFilteredRecordsMessage="Not found!"
              tableClassName={className}/>
        );
    }
  }
};

// ------------------------------------------------------------------------------


const addFilter = (props, item, e) => {
  props.addExactFilter(item, props.field.name, name = props.field.displayName);
}

const customRenderField = (props) => {
  // console.log(props);
  if (props.value && _.isString(props.value))
    return (
      <React.Fragment>
        {splitValues(props.value).map((item, index) =>
          ((item != ',') ?
            <span key={index}
                  className="filterable"
                  onClick={addFilter.bind(this, props, item)}
                  role="button">
            {item}
            </span> : ', ')
        )}
      </React.Fragment>
    );
};


function splitValues(str) {
  if (str && _.isString(str))
    return str.split(/(,)/);
  return str;
}
