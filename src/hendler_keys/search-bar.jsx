export default class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        term : ''
    }
    this.set_initial_data = false;
  }

  dataSearch (e) {
    const value = e.target.value.toLowerCase();
    const Colouns = this.props.col;
    const filter = this.load_initial_data(this.props.data).filter(function (ifaceitem) {
      return ifaceitem[Colouns].toLowerCase().includes(value);
    }.bind(Colouns));
    this.update({term:value});
    this.props.update(filter);
  }

  update(config) {
    this.setState({...this.state,...config});
  }

  load_initial_data(state) {
    if (state && !this.set_initial_data) {
      this.set_initial_data = true;
      this.initial_data = state;
      return this.initial_data
    } else return this.initial_data;
  }

  reset_filter() {
    this.update({term:''});
    this.set_initial_data = false;
  }

  componentWillReceiveProps(nextProps) {
    if (this.initial_data != nextProps.data) {
      this.reset_filter();
    }
  }
  render() {
    return (
      <div className="searchbar form-group">
        <input
          value={this.props.term}
          type="text"
          className="form-control"
          placeholder="Search ..."
          onChange={this.dataSearch.bind(this)}
        />
      </div>
    );
  }
}


