import Pagination from "react-js-pagination/dist/Pagination";

class PaginationData extends React.Component {
  constructor() {
    super();
    this.state = {
      todosPerPage: 3,
      previous: null,
      activePage: 1
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(pageNumber) {
    this.setState({...this.state,
                    previous: this.state.activePage,
                    activePage: pageNumber});
  }

  componentDidMount () {
    let activePage = (this.props.activePage) ?
      {activePage: this.props.activePage} : {};
    let perPage = (this.props.perPage) ?
      {todosPerPage: this.props.perPage} : {};
    this.setState({...this.state,
                    ...activePage,
                    ...perPage});
  }

  render() {
    const todos = (this.props.data) ? this.props.data : [];
    const {activePage, todosPerPage } = this.state;

    const indexOfLastTodo = activePage * todosPerPage;
    const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
    const currentTodos = todos.slice(indexOfFirstTodo, indexOfLastTodo);

    const renderTodos = currentTodos.map((todo, index) => {
      return JSON.stringify(todo);
    });
    return (
      <div>
        <div>
          {(this.props.render) ?
            this.props.render(currentTodos, {...this.props, ...this.state})
            :
            renderTodos}
        </div>
        {(todos.length > todosPerPage) ?
            <div id="page-numbers">
              <Pagination
                activePage={activePage}
                itemsCountPerPage={todosPerPage}
                totalItemsCount={todos.length}
                pageRangeDisplayed={5}
                onChange={::this.handleClick}
                firstPageText="First"
                nextPageText="»"
                prevPageText="«"
                lastPageText="Last"
                />
            </div> : null}
      </div>
    );
  }
}

export default PaginationData;
