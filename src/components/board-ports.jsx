import Service from './../services/service';
import IfaceImg from 'img/port1.png';
import IfaceImgActive from 'img/port2.png';
import IfaceImgN from 'img/port1_n.png';
import IfaceImgActiveN from 'img/port2_n.png';
import IfaceImgI from 'img/port_i.png';
import Logo from 'img/logo_w.svg';


const bgPorts = function (Active) {
  return {
      backgroundImage: ((Active) ? `url(${IfaceImgActive})` : `url(${IfaceImg})`)
    }
  };

const bgPortsN = function (Active) {
  return {
      backgroundImage: ((Active) ? `url(${IfaceImgActiveN})` : `url(${IfaceImgN})`)
    }
  };

const bgPort =  {
      backgroundImage:  `url(${IfaceImgI})`
  };

const Model = () => {
  return (
    <div className="model ">
      Арлан-3000
    </div>
  )
}

const BoardLogo = () => {
  return(
    <div className="indicators p-0">
      <img src={Logo} className="logo" alt="Logo" height="20" width="23"></img>
    </div>
  )
}

const BoardIndicators = () => {
  return (
    <div className="indicators p-0 mt-3">
      <div className="right-block"><br/>
        <div className="point"></div>
        Reset<br/>
        default<br/>
        <div className="point mb-2"></div>
        <div className="circle active"></div>Pwd
      </div>
    </div>
  )
}

const Popap = ({className, item}) => {
  return (
    <div className={className}>
      <div className="overlay-header"><h6>{item.iface} {item.stack_id}/{item.num_port}</h6></div>
      <p>
        Shotdown: {((item.shotdown) ? "true" : "false")}
      </p>
    </div>
  )
}

const BoardPortsAccess = () => {
  return (
    <div className="board-ports-access gradient-gray">
      <div style={bgPort} className="port mb-2" role="button">
      </div>
      <div className="port usb mb-2" role="button">
        <div className="plgn" role="button">
        </div>
      </div>
      <div className="port usb mb-2" role="button">
        <div className="plgn" role="button">
        </div>
      </div>
      <p className="port-number bottom--17 text-center">USB</p>
    </div>
  )
}

const Combo = ({Itop, Ibottom}) => {
  if (Itop && Ibottom)
  return (
    <div className="board-ports-combo">
      <p className="port-number top--17 text-center">Combo {Itop.num_port}</p>
      <div className="row">
        <div className="col col-5">
          <div className="board-ports gradient-gray">
            <a href="#">
              <div style={bgPorts(Itop.shotdown)} className="port mb-2" role="button">
              </div>
              <Popap className="overlay" item={Itop}/>
            </a>
            <a href="#">
              <div style={bgPortsN(Ibottom.shotdown)} className="port" role="button">
              </div>
              <Popap className="overlay" item={Ibottom}/>
            </a>
          </div>
        </div>
        <div className="col col-5">
          <div className="board-ports gradient-gray board-ports-stp">
            <div className="port stp mb-2" role="button">
            </div>
            <div className="port stp" role="button">
            </div>
          </div>
        </div>
        <div className="col col-2">
          <div className="indicators align-center pt-2">
            <div className="mb-0">
              act<br/>
              <div className="circle"></div>
              link
            </div>
            <div className="mb-0">
              act<br/>
              <div className="circle active"></div>
              link
            </div>
          </div>
        </div>
      </div>
      <p className="port-number bottom--17 text-center">Combo {Ibottom.num_port}</p>
    </div>
  )
}

const BoardPortsCombo = ({Interfaces}) => {
  let drop_s = drop(Interfaces);
  if (drop_s.left || drop_s.right) {
  return (
    drop_s.left.map((iface_top , key) => <Combo Itop={iface_top} Ibottom={drop_s.right[key]}/>)
  )} else
    return null
}

const BoardPorts = ({Interfaces}) => {
  let drop_s = drop(Interfaces);
  if (drop_s.left || drop_s.right) {
  return (
    <div className="board-ports gradient-gray">
      <div className="row pb-2">
        {
          drop_s.left.map((item, key) => (
            <div id={key} className="">
              <p className="port-number top--17 text-center">{item.num_port}</p>
              <a href="#">
                <div style={bgPorts(item.shotdown)} className="port" role="button">
                </div>
                <Popap className="overlay" item={item}/>
              </a>
            </div>
            )
          )
        }
      </div>
      <div className="row">
        {
          drop_s.right.map((item, key) => (
            <div id={key} className="">
              <a href="#" >
                <div style={bgPortsN(item.shotdown)} className="port" role="button">
                </div>
                <Popap className="overlay" item={item}/>
              </a>
              <p className="port-number bottom--17 text-center">{item.num_port}</p>
            </div>
            )
          )
        }
      </div>
    </div>
  )
  } else
    return null
}

const Interfaces = ({Interfaces}) => {
  if (Interfaces) {
  return (
    <div className="row ml-2 mr-2">
      <BoardPortsAccess/>
      <BoardPorts Interfaces={drop_by_iface_FaEth(Interfaces)}/>
      <BoardPortsCombo Interfaces={drop_by_iface_GbEth(Interfaces)}/>
    </div>
    )
  }
}

export default class BoardPanel extends React.Component {
   constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="col-sm-12 board">
        <div className="abs">
          <div className={"board-panel shadow-top-bottom row" +
              ((!this.props.opacity) ? " opacity-50" : "")}>
            <BoardLogo/>
            <BoardIndicators/>
            <Interfaces Interfaces={this.props.ifaces}/>
            <Model/>
            <div/>
          </div>
        </div>
      </div>
    )
  }
}


function drop(s) {
  let arr_values = {};
    arr_values.left = [];
    arr_values.right = [];
  if (s) {
    let compact_s = _.compact(s);

    arr_values.leftlen = Math.floor(compact_s.length/2);
    arr_values.rightlen = compact_s.length - arr_values.leftlen;
    compact_s.map(function (item) {
      if (item.num_port%2 == 0)
        arr_values.right.push(item);
      else
        arr_values.left.push(item);
      })
    // arr_values.left = _.dropRight(compact_s, arr_values.leftlen);
    // arr_values.right = _.drop(compact_s, arr_values.rightlen);

  }
    return arr_values;
}

function drop_by_iface_FaEth(ifaces) {
    if (ifaces) {
      return ifaces.map((iface) => (iface.iface == "fastethernet") ? iface : null)
    }
  }

function drop_by_iface_GbEth(ifaces) {
    if (ifaces) {
      return ifaces.map((iface) => (iface.iface == "gigabitethernet") ? iface : null)
    }
  }
