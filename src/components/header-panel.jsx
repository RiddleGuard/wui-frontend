import Service from './../services/service';

export default class HeaderPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false
    }
  }

  handle_btn_logout(e) {
    Service
    .logout()
    .then(function(res) {
          alert(res);
          location.reload();
      }, function(error) {
        alert(error)
      });
  }

  handle_btn_refresh(e) {
    if (this.props.timer && this.props.timer.isRefresh)
      this.props.timer.stop();
    else {
      this.props.timer.init();
    }
    this.setState({active: this.props.timer.isRefresh});
    return 0;
  }

  is_active() {
     return (this.state.active) ? "active" : null;
  }

  render() {
    return(
      <div className="text-right header-panel">
        <button href="#"
          onClick={this.handle_btn_logout}
          className="btn btn-outline-secondary" role="button">
          <i className="fa fa-sign-out" aria-hidden="true"> Logout</i>
        </button><br/>
        <div className="btn-group" role="group">
          <button onClick={this.props.timer && this.props.timer.func}
            className="btn btn-outline-secondary" role="button">
            <i className="fa fa-refresh" aria-hidden="true"></i>
          </button>
          <button onClick={this.handle_btn_refresh.bind(this)}
            className={"btn btn-outline-secondary " + this.is_active()}
            role="button">
            <i className="fa fa-cogs" aria-hidden="true"> Auto</i>
          </button>
        </div>
      </div>
    );
  }
}
