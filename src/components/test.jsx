import {LineChart, AreaChart, Line, Area, XAxis, YAxis, ReferenceLine,
CartesianGrid, Tooltip, Legend, Brush} from 'recharts';

export default class NotFound extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
      {Date: 'Page A', uv: 4000, pv: 2400, amt: 2400},
      {Date: 'Page B', uv: 3000, pv: 1398, amt: 2210},
      {Date: 'Page C', uv: 2000, pv: 9800, amt: 2290},
      {Date: 'Page D', uv: 2780, pv: 3908, amt: 2000},
      {Date: 'Page E', uv: 1890, pv: 4800, amt: 2181},
      {Date: 'Page F', uv: 2390, pv: 3800, amt: 2500},
      {Date: 'Page G', uv: 3490, pv: 4300, amt: 2100}],
      startI: 0,
      endI: 0
    };
  }

  handler({startIndex, endIndex}) {
    let arr = {};
    this.handlerUpdata({startIndex, endIndex}).bind(this);
    return {startIndex, endIndex};
  }

  handlerUpdata({startIndex, endIndex}) {
    this.setState({...this.state, ...{startI: startIndex, endI: endIndex}});
    return {startIndex, endIndex};
  }

  // componentWillReceiveProps(nextProps) {
  // }

  handlerButton(e) {
    this.setState({...this.state,
                   data: [...this.state.data, {Date: 'Page H', uv: 3490, pv: 4300, amt: 5100}],
                   startI: this.state.startI,
                   endI: this.state.endI
    });
  }

  render () {
    return (
      <div>
        <button onClick={this.handlerButton.bind(this)}>Нямка</button>
        <LineChart width={600}
                   height={300}
                   data={this.state.data}
                   margin={{top: 20, right: 50, left: 20, bottom: 5}}>
         <XAxis dataKey="Date"/>
         <YAxis/>
         <CartesianGrid strokeDasharray="3 3"/>
         <Tooltip/>
         <Legend />
         <ReferenceLine x="Page C" stroke="red" label="Max PV PAGE"/>
         <ReferenceLine y={9800} label="Max" stroke="red"/>
         <Line type="monotone" dataKey="amt" stackId="1" stroke="#8884d8" fill='#8884d8'/>
         <Line type="monotone" dataKey="pv" stroke="#8884d8" />
         <Line type="monotone" dataKey="uv" stroke="#82ca9d" />
         <Brush dataKey="Date"
                height={20}
                startIndex={this.state.startI}
                endIndex={this.state.endI}
                data={this.state.data}
                onChange={this.handlerUpdata.bind(this)}/>
        </LineChart>
      </div>
    );
  }
}
