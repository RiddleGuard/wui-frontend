
export default class NotFound extends React.Component {
  render() {
    return (
      <div className="col-sm-12 col-md-10 panel-info p-5 content">
        <h3><strong>404 page not found</strong>
          <hr/></h3>
        <div>
          <p>We are sorry but the page you are looking for does not exist.</p>
        </div>
      </div>
    );
  }
}
