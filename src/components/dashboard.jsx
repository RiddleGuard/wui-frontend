import Service from './../services/service';
import Window from './../hendler_keys/window';
import _ from 'lodash';

export default class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      left: {Width: 1000+'px'}
    };

  }

  get_page() {
    Service
    .get_dashboard()
    .then((res) => this.props.update({page: res}))
  }

  componentWillMount() {
    this.get_page();
  }

  render() {
    return (
      <div className="col-sm-12 col-md-10 panel-info p-5 content">
        <h3><strong>{this.props.title}</strong>
          <hr/></h3>
        <div >
          <div className="row ">
            <div className="col-sm-12 col-md-8 col-lg-9 p-3">
              <Window rows={this.props.device_info} name=" Device info"
                      config={{toggle: false}}
              classNameIcon="fa fa-info-circle fa-1x"
              className="dashboard table-sm-no"/>
            </div>
            <div className="col-sm-12 col-md-4 col-lg-3 p-3">
              <Window rows={this.props.log_stat} name=" Log statistics"
                      config={{toggle: false}}
              classNameIcon="fa fa-bar-chart fa-1x"
              className="dashboard"/>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12">
              <div className="dashboard">
              <h5><span className="fa fa-info-c+rcle fa-1x"></span> Device sub information</h5>
                {this.render_device_sub_info()}
              </div>
            </div>
          </div>

            {this.render_devices()}

        </div>
      </div>
    );
  }

  render_device_sub_info() {
    if (this.props.device_sub_info) {
      let len = this.props.device_sub_info.length;
      let leftlen = Math.floor(len/2);
      let rightlen = len - leftlen;
      let left = _.dropRight(this.props.device_sub_info,leftlen);
      let right = _.drop(this.props.device_sub_info,rightlen);

      return (
        <div className="row">
          <div className="col-sm-12 col-md-6 pr-md-0">
            <table className="table table-striped table-hover table-sm">
              <tbody>
                {
                  left.map((item) => (
                    <tr>
                      <th >{item.name}</th>
                      <th width="182px">{item.val}</th>
                    </tr>
                    ))
                }
              </tbody>
            </table>
          </div>
          <div className="col-sm-12 col-md-6 pl-md-0 ">
            <table className="table table-striped table-hover table-sm">
              <tbody>
                {
                  right.map((item) => (
                    <tr>
                      <th >{item.name}</th>
                      <th width="182px">{item.val}</th>
                    </tr>
                    ))
                }
              </tbody>
            </table>
          </div>
        </div>
      )
    }
  }

  render_devices() {
  if (this.props.devices) {

  return this.props.devices.map((item_device) =>
      <div className="row">
        <div className="col-sm-12 col-md-6 col-lg-5 pt-3 pl-3 pr-3 pb-0">
          <div className="dashboard">
            <h5><span className="fa fa-info-circle fa-1x"></span> Device</h5>
            <table className="table table-striped table-hover table-sm">
              <tbody>
                {(() => {
                  if (item_device.device) {
                    return item_device.device.map((item) =>
                      <tr>
                        <th >{item.name}</th>
                        <th>{item.val}</th>
                      </tr>
                    )
                  }
                })()}
              </tbody>
            </table>
          </div>
        </div>
        <div className="col-sm-12 col-md-6 col-lg-7 pt-3 pl-3 pr-3 pb-0">
          <div className="dashboard">
            <h5><span className="fa fa-bar-chart fa-1x"></span> CPU and Utilization</h5>
            {(() => {
              if (item_device.cpu_and_util) {
              return (
                <div className="row">
                  <div className="col-2">
                    <div className="util o-hidden p-0">
                      <table id="q-graph">
                        <tbody>
                          <tr>
                            <td className="sent bar" style={{height: item_device.cpu_and_util.cpu+'%'}}>{item_device.cpu_and_util.cpu}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div className="col-10">
                    <div className="util">
                      <table id="q-graph" style={this.state.left} style={{width: item_device.cpu_and_util.util.length * 50 + 'px'}}>
                        <thead>
                          <tr>
                          <th></th>
                          <th className="sent">Rx</th>
                          <th className="paid">Tx</th>
                          </tr>
                        </thead>
                        <tbody>
                          {(() => {
                            return item_device.cpu_and_util.util.map((item,key) =>
                              <tr className="qtr" style={{left: key*50 + 'px' }}>
                                <th scope="row">{item.val[0]}</th>
                                <td className="sent bar" style={{height: item.val[1] + '%'}}><p className="padding-top">{item.val[1]}</p></td>
                                <td className="paid bar" style={{height: item.val[2] + '%'}}><p className="padding-top">{item.val[2]}</p></td>
                              </tr>
                            )
                          })()}

                        </tbody>
                      </table>

                      <div id="ticks" style={this.state.left}>
                        <div className="tick" style={{height: 59, width: item_device.cpu_and_util.util.length * 50 + 'px'}}><p>100%</p></div>
                        <div className="tick" style={{height: 59, width: item_device.cpu_and_util.util.length * 50 + 'px'}}><p>80%</p></div>
                        <div className="tick" style={{height: 59, width: item_device.cpu_and_util.util.length * 50 + 'px'}}><p>60%</p></div>
                        <div className="tick" style={{height: 59, width: item_device.cpu_and_util.util.length * 50 + 'px'}}><p>40%</p></div>
                        <div className="tick" style={{height: 59, width: item_device.cpu_and_util.util.length * 50 + 'px'}}><p>20%</p></div>
                        <div className="tick" style={{height: 59, width: item_device.cpu_and_util.util.length * 50 + 'px'}}><p>0</p></div>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            })()}
          </div>
        </div>
      </div>
    )
  }
  }
}
