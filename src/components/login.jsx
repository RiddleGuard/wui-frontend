import {findDOMNode} from 'react-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import Service from './../services/service';

export default class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    let loginData = {};
    loginData.username = findDOMNode(this.refs.username).value.trim();
    loginData.password = findDOMNode(this.refs.password).value.trim();

    Service.login(loginData).then(function(res) {
      alert(res.info)
      location.replace('/');
    }, function(error) {
      alert(error.info)
    });

  }

  render() {
    return (
      <div className="">
        <div className="loginmodal-container">
            <h1>Login</h1><br/>
            <form action="#">
              <input key="username" type="text" ref='username' placeholder="Username"/>
              <input key="password" type="password" ref='password' placeholder="Password"/>
            </form>
            <button onClick={this.handleSubmit} className="login loginmodal-submit">Login</button>
        </div>
      </div>
    );
  }
}
