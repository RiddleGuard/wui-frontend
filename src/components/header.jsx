import BoardPanel from './board-ports';
import HeaderPanel from './header-panel';
import Service from './../services/service';
import Logo from 'img/logo.png';

const Logotype = () => {
  return(
    <h1 className="center">
      <img src={Logo} alt="Logo" width="180"></img>
    </h1>
  );
}

export default class Header extends React.Component {
   constructor(props) {
    super(props);
    this.state = {
      ifaces : []
    };
  }

   componentDidMount() {
   Service
      .get_if_status()
      .then((res) => this.setState({...this.state, ifaces:res}));
  }

  render() {
    return (
      <div className="header row p-2 pl-4 pr-4">
        <div className="col-sm-12 col-md-2 pl-0"><Logotype/></div>
        <div className="col-sm-12 col-md-8">{/*<BoardPanel {...this.state}/>*/}</div>
        <div className="col-sm-12 col-md-2"><HeaderPanel {...this.props}/></div>
      </div>
    );
  }
};
