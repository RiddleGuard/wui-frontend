import { Switch, Route } from 'react-router-dom';
import Q from 'q';
import Service from './../services/service';
import Sidebar from './sidebar';
import Header from './header';
import Dashboard from './dashboard';
import StackDevices from './stack-devices';
import NotFoundPage from './not-found-page';
import Test from './test';
import _ from 'lodash';
import { set } from './../utils/utils'
import Timer from "./class/timer";
import Query from "./class/query";


export default class Content extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: [],
      isLoading: false
    };
    this.timer = null;
    this.queryPage = new Query();
    this.eventUpdata = new Event("UPDATE");
  }

  refresh() {
    let value, url;
    this.queryPage.setQuery(this.props.match.params.not_found_page);
    url = this.queryPage.getQuery();
    value = this.queryPage.getQueryValues();
    this.get_page(url, value);
  }

// -----------------------------------------------------------------------------
  get_page(path, val = {}) {
    let deferred = Q.defer();
    this.update_data({isLoading: true});
    Service
    .get_data(path, val)
    .then(res => deferred.resolve(this.updata_page(res), val),
          err => deferred.reject({status: err}))
    return deferred.promise;
  }

  updata_page(res) {
    // @todo на самом деле это должно отправлятся в область контент
    // это событие относится к их модулям
    document.dispatchEvent(this.eventUpdata);
    this.update_data({page: res, isLoading: false});
    return 0;
  }

// промис нигде не используются
  get_obj(path, val = {}) {
    this.queryPage.addQueryValue(set(val.name, val));
    let deferred = Q.defer();
    this.update_data({isLoading: true});
    Service
    .get_data(path, this.queryPage.getQueryValues())
    .then(res => deferred.resolve(this.updata_page(res), val),
          err => deferred.reject({status: err}))
    return deferred.promise;
  }

  local_updata_page(res, val) {
    let page = replace_obj(val.link, res[val.link], this.state.page);
    this.update_data({page: page});
    return 0;
  }
// -----------------------------------------------------------------------------

  update_data(up_data) {
    this.setState({...this.state,...up_data});
  }

  componentWillUnmount() {
    this.queryPage.clear();
    this.timer.clear();
  }

  componentDidMount() {
    this.timer = new Timer(this.refresh.bind(this));
    this.timer.setInterval(3000); // 3 сек
    this.queryPage.addQuery(this.props.match.params.not_found_page);
  }

  render() {
    return (
      <div className="container-fluid">
        <Header timer={(this.timer) ? this.timer : null}/>
        <div className="middle row ">
          <Sidebar/>
          <Switch>
            <Route path='/dashboard'
                   render={() =>
                     <Dashboard {...this.state.page}
                                update={this.update_data.bind(this)}/>}/>
            <Route path='/stack_devices'
                   render={() =>
                     <StackDevices {...this.state.page}
                                update={this.update_data.bind(this)}/>}/>
            <Route path='/test'
                   render={({match}) =>
                     <Test match={match}/>}/>
            <Route path="/:l2/:not_found_page"
                   render={({ match }) =>
                     <NotFoundPage data={this.state.page}
                                   isLoading={this.state.isLoading}
                                   match={match}
                                   query={this.queryPage}
                                   get_page={this.get_page.bind(this)}
                                   get_obj={this.get_obj.bind(this)}/>}/>
            <Route path="/:not_found_page"
                   render={({ match }) =>
                     <NotFoundPage data={this.state.page}
                                   isLoading={this.state.isLoading}
                                   match={match}
                                   query={this.queryPage}
                                   get_page={this.get_page.bind(this)}
                                   get_obj={this.get_obj.bind(this)}/>}/>
          </Switch>
        </div>
      </div>
    );
  }
}

function get_key (key, json) {
  let result = {};
  for (let json_key in json)
    if (json_key == key && isHkey(json[json_key]))
      return json[json_key]
    else
      if (_.isArray(json[json_key]) || _.isObject(json[json_key])) {
        result = get_key (key, json[json_key]);
        if (result != null && isHkey(result))
          return result
      }
  return null;
}

function prefix (key, Value) {
  return '"' + key + '":' + Value
}

function  replace_obj (key1, value, json, key2 = key1) {
  let stringified = JSON.stringify(json);
  let ch = prefix (key1, JSON.stringify(get_key (key1, json)));
  let na = prefix (key2, JSON.stringify(value));
  stringified = stringified.replace(ch, na);
  return JSON.parse(stringified);
}

// ABCvABCDvABCEvABCDE == ABC(1vDvE(1vD))
function isHkey(obj) {
  if ( (obj['type'] &&
        obj['namespace'] &&
        obj['data']) && (
        obj['config'] ||
        ( obj['className'] &&
          ( bj['config'] || true ) ) ||
        true) )
    return true;
  else
    return false;
}
