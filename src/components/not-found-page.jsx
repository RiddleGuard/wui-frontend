import HKey from './../hendler_keys/hkey';
import {data6,data7,data5,data4,data3,data2} from './../hendler_keys/_TESTS_/dropdown-list-spec';

export default class NotFoundPage extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (this.props.match) {
      this.props.get_page('/' + this.props.match.params.not_found_page);
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.match && (this.props.match.params.not_found_page != nextProps.match.params.not_found_page)) {
      //если выбрана другая страница
      this.props.query.clearVlaue();
      this.props.get_page('/' + nextProps.match.params.not_found_page)
    };
  }

  title() {
    if (!this.props.match)
      return "no title"
    else if (this.props.data && !this.props.data.title)
        return this.props.match.params.not_found_page
      else
        return this.props.data.title
  }

  local_updata (val = {}, data = this.props.data) {
    if (this.props.match && this.props.match.params.not_found_page) {
      this.props.get_obj('/' + this.props.match.params.not_found_page, val);
    }
  }

  render() {
    return (
      <div className="col-sm-12 col-md-10 panel-info p-5 content">
        <h3><strong>{this.title()}</strong>
          <hr/></h3>
        <div>
          {(!(this.props.data && (!this.props.data || this.props.data.title))) ?
          <p>We are sorry but the page you are looking for does not exist.</p> :
          <HKey data={this.props.data}
                updata={this.local_updata.bind(this)}
                isLoading={this.props.isLoading}/>
          }
        </div>
      </div>
    );
  }

}
