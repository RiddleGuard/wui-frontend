import Service from './../services/service';
import BoardPanel from './board-ports';

export default class StackDevices extends React.Component {
  constructor(props) {
    super(props);
  }

  get_devices() {
    Service
    .get_devices()
    .then((res) => this.props.update({page: {ifaces: res}}))
  }

  componentDidMount() {
    this.get_devices();
  }

  render() {
    return(
      <div className="col-sm-12 col-md-10 panel-info p-5 content">
        <h3><strong>{this.props.title}</strong>
          <hr/></h3>
        <div>
          {this.props.ifaces &&
            _.map(this.props.ifaces,
              (ifaces, index) =>
                  <div key={index} className="ml-5 mr-5 mb-3 mt-3">
                    Stack_id: {index}
                    <BoardPanel ifaces={ifaces} opacity/>
                  </div>)}
        </div>
      </div>
    )
  }
}
