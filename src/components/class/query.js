// CLass Query >>>
function Query() {
    this.query = '';
    this.value = {};
}
Query.prototype.setQuery = function(query) {
  this.query = `/${query}`;
  return 0;
};
Query.prototype.addQuery = function(query) {
  this.query = `${this.query}/${query}`;
  return 0;
};
Query.prototype.addQueryValue = function(value) {
  this.value = {...this.value, ...value};
  return 0;
};
Query.prototype.getQuery = function() {
  return this.query
};
Query.prototype.getQueryValues = function() {
  return this.value;
};
Query.prototype.clearVlaue = function() {
    this.value = {};
  return 0;
};
Query.prototype.clear = function() {
    this.query = '';
    this.value = {};
  return 0;
};
// CLass Query <<<
export default Query;
