// CLass Timer >>>
function Timer(func) {
  this.isRefresh = false;
  this.interval = 10000;
  this.timerId = null;
  if (typeof func == "function") {
    this.func = func
  } else
    throw `value ${func} is not function`;
  return 0;
}

Timer.prototype.init = function() {
  this.clear();
  this.isRefresh = true;
  this.loop();
  return 0;
}

Timer.prototype.stop = function() {
  this.clear();
  this.isRefresh = false;
  return 0;
}

Timer.prototype.loop = function() {
  this.timerId = setInterval(() => this.func(), this.interval);
  return 0;
}

Timer.prototype.clear = function() {
  if (this.timerId) clearInterval(this.timerId);
  return 0;
}

Timer.prototype.setInterval = function(value) {
  this.interval = value;
  return 0;
}

Timer.prototype.getInterval = function() {
  return this.interval
};
// CLass Timer <<<
export default Timer;
