import { Link } from 'react-router-dom';
import Service from './../services/service';

export default class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menu: []
    };

  }
  //вызывается перед рендером
  componentWillMount() {
    Service.get_menu().then((res) => this.setState({...this.state, menu:res}));
  }

  render_menu(Menu, item_link='') {
    if (Menu) {
      return (
        Menu.map(function(item) {
          if (item.active) {
            let link = `${item_link}/${item.name}`;
            if (item.children) {
              return this.render_sub_menu_item(item, link, item.name);
            } else {
              return this.render_menu_item(item, link);
            }
          }
        }.bind(this))
      )
    }
  }

  render_sub_menu_item(item, link, name) {
    return(
    <div>
      <span data-toggle="collapse" data-target={`#${name}`}>
          <li><a >{item.text}</a></li>
      </span>
      <ul id={name} className="collapse">
        {this.render_menu(item.children, link)}
      </ul>
    </div>);
  }

  render_menu_item(item, link) {
    return (<li><Link to={link}>{item.text}</Link></li>);
  }

  render() {
    return (
      <div className="sidebar-left col-sm-12 col-md-2 pb-5 pt-5 pl-4 pr-3">
        <div className="menu">
          <ul className="pl-0" id="accordion">
            {this.render_menu(this.state.menu)}
          </ul>
        </div>
      </div>
    );
  }
}
