import 'babel-polyfill'
import ReactDOM from 'react-dom';
import Content from './components/content';
import LoginForm from './components/login';
import NotFound from './components/not-found';
import { Router } from 'react-router-dom';
import { Switch, Route ,Redirect} from 'react-router-dom';
import AuthTokenManager from './utils/auth-token-manager';
import createBrowserHistory from 'history/createBrowserHistory';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import 'muicss/dist/css/mui-noglobals.min.css';
import 'css/loader.css';
import 'css/graph.css';
import 'css/dialog.css';
import 'css/pagination.css';
import 'css/table.css';
import 'css/board-ports.css';
import 'css/tabs.css';
import 'css/ddl.css';
import 'css/ddt.css';
import 'css/style.css';
const customHistory = createBrowserHistory();


if ((AuthTokenManager.getToken() == null))
    customHistory.push('/login')

ReactDOM.render(
  <Router history={customHistory}>
    <Switch>
      <Route path='/login' render={() => <LoginForm/>}/>
      <Route path='/:l2/:not_found_page'
             render={({match}) => <Content match={match}/>}/>
      <Route path='/:not_found_page'
             render={({match}) => <Content match={match}/>}/>
      <Redirect from='/' to='/dashboard'/>
      <Route path="*" component={NotFound} />
    </Switch>
  </Router>,
  document.getElementById('root')
);
