import Q from 'q';
import _ from 'lodash';
import AuthTokenManager from './../utils/auth-token-manager';
import request from 'superagent';

const Service = {

  login (Data) {
    let deferred = Q.defer();
    let json = { username: Data.username , password: Data.password };
    request
      .post('/asynch/login')
      .send(json)
      .end(function(err, res) {
        if (_.isEmpty(err) && res.body.access_token){
          AuthTokenManager.setToken(res.body.access_token);
          deferred.resolve(res.body);
        }
        else {
          deferred.reject(res.body);
        }
    });
    return deferred.promise;
  },

  logout () {
    let deferred = Q.defer();
    if (_.isEmpty(AuthTokenManager.removeCookie())){
      deferred.resolve("ok");
    }
    else {
      deferred.reject("error");
    }
    return deferred.promise;
  },

  get_data(name, val = {}) {
    let deferred = Q.defer();
    request
      .get('/asynch' + name)
      .set( 'Authorization' , AuthTokenManager.getToken())
      .query(val)
      .end(function(err, res){
         if (!_.isEmpty(res) && _.isEmpty(res.body.err)){
          deferred.resolve(res.body);
         } else {
          if (res && res.body && res.body.err == "not_found_token") {
            deferred.reject(this.logout());
            location.replace('/login');
          }
          else deferred.reject(res.body.err);
         }
      }.bind(this))
    return deferred.promise;
  },

  get_if_status(){
    let deferred = Q.defer();
    this.get_data('/iface-status')
      .then((res) => deferred.resolve(res), (err) => deferred.reject(err));
      return deferred.promise;
  },

  get_menu(){
    let deferred = Q.defer();
    this.get_data('/menu')
      .then((res) => deferred.resolve(res), (err) => deferred.reject(err));
      return deferred.promise;
  },

  get_dashboard() {
    let deferred = Q.defer();
    this.get_data('/dashboard')
      .then((res) => deferred.resolve(res), (err) => deferred.reject(err));
      return deferred.promise;
  },

  get_devices() {
    let deferred = Q.defer();
    this.get_data('/iface-status/stack_devices')
      .then((res) => deferred.resolve(res), (err) => deferred.reject(err));
      return deferred.promise;
  }
};

export default  Service;
