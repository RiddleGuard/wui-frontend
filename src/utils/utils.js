const stringConstructor = "text".constructor;
const arrayConstructor = [].constructor;
const objectConstructor = {}.constructor;

function json_type_of(object) {
  if (object === null) {
    return "null";
  }
  else if (object === undefined) {
    return "undefined";
  }
  else if (object.constructor === stringConstructor) {
    return "String";
  }
  else if (object.constructor === arrayConstructor) {
    return "Array";
  }
  else if (object.constructor === objectConstructor) {
    return "Object";
  }
  else {
    return "don't know";
  }
}

function set(key, Values) {
  if (key != undefined) {
    let obg = {} ;
    obg[key]=Values;
    return obg;
  } else {
    return undefined;
  }
}


export { json_type_of, set };
