const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    app: './src/index.jsx'
  },
  plugins: [
    // new HtmlWebpackPlugin({
    //   title: 'Production'
    // })
    new webpack.ProvidePlugin({
         $: 'jquery',
        '$':          'jquery',
        'React':   'react',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        Popper: ['popper.js', 'default'],
        Util: "exports-loader?Util!bootstrap/js/dist/util",
        Dropdown: "exports-loader?Dropdown!bootstrap/js/dist/dropdown",
      }),
    new webpack.NoEmitOnErrorsPlugin(),
    new ExtractTextPlugin('css/bundle.css'),
    new webpack.DefinePlugin({ "global.GENTLY": false }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './public/index.html'
    })
  ],
  output: {
    filename: 'js/bundle.js',
    path: path.resolve(__dirname, './dist/'),
    publicPath: '/'
  },
  resolve: {
    modules: ['node_modules','public'],
    mainFiles: ['*', 'index'],
    extensions: [".js",".jsx", ".json"]
  },
  node: {
    __dirname: true,
  },
  module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          include: [
                      path.resolve(__dirname, 'src')
                  ],
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                ['env', {"modules": false }],
                'stage-0', 'react'],
              plugins: ['transform-runtime',
                'transform-react-constant-elements',
                'transform-react-inline-elements']
            }
          }
        },
        {
          test: /\.(scss)$/,
          use: [{
            loader: 'style-loader', // inject CSS to page
          }, {
            loader: 'css-loader', // translates CSS into CommonJS modules
          }, {
            loader: 'postcss-loader', // Run post css actions
            options: {
              plugins: function () { // post css plugins, can be exported to postcss.config.js
                return [
                  require('precss'),
                  require('autoprefixer')
                ];
              }
            }
          }, {
            loader: 'sass-loader' // compiles Sass to CSS
          }]
        },
        {
          test: /\.(jpe?g|png|gif|svg)$/i,
          use: [
            'url-loader?limit=10000',
            'img-loader'
          ]
        },
        {
          test: /\.css$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: 'css-loader'
          })
        },
        {
         test: /.(ttf|otf|eot|woff(2)?)(\?[a-z0-9]{1:3})?$/,
         use: [{
           loader: 'file-loader',
           options: {
             name: '[name].[ext]',
             outputPath: 'fonts/',
             publicPath: './../fonts'
           }
         }]
       },
      ]
    }
};
