const webpack = require('webpack');
const path = require('path');
const NODE_ENV = process.env.NODE_ENV || 'development';
const CleanWebpackPlugin = require('clean-webpack-plugin');
const merge = require('webpack-merge');
const common = require('./webpack.common.config.js');

module.exports = merge(common, {
    entry: [
          "./src/index.jsx"
    ],
    watch: NODE_ENV == 'development',
    watchOptions: {
        aggregateTimeout: 100
    },
    devtool: (NODE_ENV == 'development') ? 'cheap-inline-module-source-map' : false,
    plugins: [
      new CleanWebpackPlugin(['dist'])
    ],
    devtool: 'inline-source-map',
    devServer: {
      host: 'localhost',
      port: 3000,
      contentBase: __dirname + '/public',
      inline: true,
      hot: false,
      historyApiFallback: true
    }
  }
);
