const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common.config.js');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const PATH_BACKEND =
  process.env.PATH_BACKEND || path.join(__dirname, '../backend');

var createWebpackConfig = function(outputPath) {
  return {
    output: {
      path: path.join(outputPath, '/priv'),
      publicPath: '/'
    },
    resolveLoader: {
      modules: ['node_modules','public'],
      extensions: [".jsx",".js", ".json"],
      moduleExtensions: ['-loader'],
      mainFields: ["loader", "main"],
    },
    plugins: [
      new CleanWebpackPlugin(['priv'], {
          root:     outputPath,
          verbose:  true,
          dry:      false
        }),
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production')
      }),
      new webpack.optimize.UglifyJsPlugin({
        uglifyOptions: {
          output: {
            beautify: false,
            comments: false,
          },
          compress: {
            sequences     : true,
            booleans      : true,
            loops         : true,
            unused      : true,
            warnings    : false,
            drop_console: true,
            unsafe      : true
          }
        }
      }),
      new webpack.optimize.CommonsChunkPlugin({
        children: true,
        async: true,
      }),
      new webpack.optimize.OccurrenceOrderPlugin()
    ]
  };
}

module.exports = merge(common, createWebpackConfig(PATH_BACKEND));
